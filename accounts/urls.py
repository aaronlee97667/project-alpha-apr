from django.urls import path
from .views import login_account, logout_user, signup


urlpatterns = [
    path("login/", login_account, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup, name="signup"),
]
