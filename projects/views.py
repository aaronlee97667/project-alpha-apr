from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm

# from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def list_projects(request):
    project_instance = Project.objects.filter(owner=request.user)
    context = {"project_instance": project_instance}
    return render(request, "projects/project_list.html", context)


@login_required
def show_project(request, id):
    project_details = Project.objects.get(id=id)
    context = {
        "project_details": project_details,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
